<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Articulo;
use app\models\Foto;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
     public function actionIndex()
    {   
         $dataProvider = new ActiveDataProvider([
            'query' => Articulo::find(),
             'pagination'=>[
                 'pageSize'=>1
             ]
        ]);
         
        $datos= Articulo::find()->all();
        return $this->render('index',[
            "dataProvider"=>$dataProvider,
            "articulos"=>$datos,
        ]);
    }
    
    public function actionEjemplo()
    {   
        $datos= Articulo::find()->all();
        return $this->render('index',[
            "articulos"=>$datos,
        ]);
    }

    
    public function actionFotosarticulo()
    {   
        $datos= Articulo::find()->all();
        return $this->render('fotosArticulo',[
            "articulos"=>$datos,
        ]);
    }
    
    public function actionBack(){
    
        return $this->render('back');
    }
        public function actionListar(){
        $listado= Articulo::find();
        $pagination = new Pagination([
            'defaultPageSize' => 1,
            'totalCount' => $listado->count(),
        ]);
        
        $articulos = $listado->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();
        
        $fotos=Foto::find()->all();
        return $this->render("listado",[
            "datos"=>$articulos,
            "fotos"=>$fotos,
            "paginas"=>$pagination
        ]);
    }

}
