DROP DATABASE IF EXISTS aplicacionbasica4;
CREATE DATABASE aplicacionbasica4;
USE aplicacionbasica4;

CREATE TABLE articulo(
  id int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(255) NOT NULL,
  texto text DEFAULT null,
  PRIMARY KEY(id)
  );

CREATE TABLE foto(
  id int(11) NOT NULL AUTO_INCREMENT,
  articulo int NOT NULL,
  nombre varchar(255) DEFAULT NULL,
  alt varchar(255) DEFAULT NULL,
  PRIMARY KEY(id),
  UNIQUE KEY(articulo,nombre),
  CONSTRAINT FKFotoArticulo FOREIGN KEY(articulo) REFERENCES articulo(id) 
  );