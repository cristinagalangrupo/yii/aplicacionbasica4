<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articulos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Articulo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                
            ],

            'id',
            'titulo',
            'texto:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}{miBoton}',
                'buttons'=>['miBoton'=>function($url,$model,$key){
                        return Html::a(Html::img('@web/imgs/icon.png',['width'=>'20px']), ['site/fotosarticulo',], ['class' => 'profile-link']);
                }] 
                ],
        ],
    ]); ?>
</div>
