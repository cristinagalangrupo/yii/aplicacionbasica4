<?php 
use yii\helpers\Html;
?>
<ul class="nav nav-pills nav-stacked">
 
  <li role="presentation"><?= Html::a('Artículos', ['articulo/index',], ['class' => 'btn btn-info','style'=>'font-size:30px']) ?></li>
  <li role="presentation"><?= Html::a('Fotos', ['foto/index',], ['class' => 'btn btn-info','style'=>'font-size:30px;']) ?></li>
</ul>
