<div>
<h2 class="alert-info alert">listado de artículos</h2>

<?php
echo yii\widgets\LinkPager::widget([
    'pagination' => $paginas,
]);

foreach($datos as $dato){
    echo $this->render('_articulo',[
        "dato"=>$dato,
        "fotos"=>$fotos
    ]);
}
echo yii\widgets\LinkPager::widget([
    'pagination' => $paginas,
]);

?>

</div>