<?php
use yii\base\Widget;
use yii\grid\GridView;
use app\models\Foto;
// var_dump($articulos);
/* @var $this yii\web\View */

$this->title = 'Listado';
?>
<div class="site-index">
    <ul>
        <?php 
        foreach($articulos as $articulo){
            echo $this->render('index/_articulo',[
                "datos"=>$articulo, // es como el ejemplo de los autores
            ]);
            /**
             * Del artículo actual quiero ver las fotos
             */
            $fotosArticulo=$articulo->fotos; // es un array de modelos Foto
            
            foreach ($fotosArticulo as $foto){
                echo $this->render("index/_fotos",[
                "datos"=>$foto,
            ]);
            }
            
            
            /*
            $a=$articulo->fotos;
            var_dump($a);
            
            $a=Foto::find()
                    ->where(["articulo"=>$articulo->id])
                    ->all();
            var_dump($a);
             * */
             
        }
        ?>
    </ul>
  
</div>
